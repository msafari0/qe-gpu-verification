#!/usr/bin/env python

#
# Maintainer: Pietro Bonfa'
#

import sys
from aiida.orm import Group, Code, DataFactory, load_node, JobCalculation
from aiida.orm.querybuilder import QueryBuilder
from aiida.tools.dbimporters.plugins.cod import CodDbImporter
importer = CodDbImporter()

def run(codenames):
    StructureData = DataFactory('structure')
    ParameterData = DataFactory('parameter')
    KpointsData = DataFactory('array.kpoints')
    ###############################
    # This pseudo are from 
    # https://www.physics.rutgers.edu/gbrv/all_pbe_UPF_v1.5.tar.gz
    pseudo_family = 'gbrv-pbe-1.5'
    ###############################

    results = importer.query(id=1010586)
    s = results[0].get_aiida_structure()
    parameters = ParameterData(dict={
              'CONTROL': {
                  'calculation': 'scf',
                  'restart_mode': 'from_scratch',
                  'wf_collect': True,
                  },
              'SYSTEM': {
                  'ecutwfc': 30.,
                  'ecutrho': 300.,
                  'nspin': 2,
                  'starting_magnetization': {'Mn': 0.5},
                  'smearing': 'mp',
                  'occupations': 'smearing',
                  'degauss': 0.01,
                  },
              'ELECTRONS': {
                  'conv_thr': 1.e-6,
                  'mixing_beta': 0.5,
                  }})
    
    kpoints = KpointsData()
    kpoints.set_kpoints_mesh([2,2,2])
    
    for codename in codenames:
        code = Code.get_from_string(codename)
        calc = code.new_calc(max_wallclock_seconds=7200,
            resources={"num_machines": 1})
        calc.label = "ManganeseOxide"
        calc.description = "Testing pw.x with Manganese Oxide: kpoint, ultrasoft"
        
        calc.use_structure(s)
        calc.use_code(code)
        calc.use_parameters(parameters)
        calc.use_kpoints(kpoints)
        calc.use_pseudos_from_family(pseudo_family)
        
        calc.store_all()
        print ("created calculation with PK={}".format(calc.pk))
        group, created = Group.get_or_create(name="MnO")
        group.add_nodes(calc)
        calc.submit()



if __name__ == '__main__':
    if len(sys.argv) >= 1:
        run(sys.argv[1:])
    else:
        print("Usage: {} codename@machine [codename2@machine2 ...]".format(sys.argv[0]))

